module.exports = {
  app: {
    root: 'http://localhost',
    port: 3005,
    playground: '/',
    voyager: '/voyager',
    endpoint: '/graphql',
    subscriptions: '/subscriptions',
  }
}
