const jwt = require('jsonwebtoken')
const db = require('../utils/db')
const bcrypt = require('bcryptjs')

module.exports = {
  Query: {
    authenticateAccount: async (_parent, args, context, _info) => {
      const { accountInput } = args
      const authenticatedAccount = await db.collection('q-auth').findOne({ email: accountInput.email })
      if (!authenticatedAccount) return null
      const passwordVerified = await new Promise((resolve, reject) => {
        bcrypt.compare(accountInput.password, authenticatedAccount.password, function (err, res) {
          if (res) {
            resolve(res)
          } else {
            resolve(err)
          }
        })
      })
      if (!passwordVerified) return null
      const token = jwt.sign({ id: authenticatedAccount._id, accountType: authenticatedAccount.accountType, issuedAt: new Date() }, process.env.AUTH_KEY || "jwtQ", { algorithm: 'HS256' })
      return { 'authToken': token }
    }
  },
  Mutation: {
    createAuthAccount: async (_parent, args, _context, _info) => {
      const { accountInputWithId } = args
      const existingAccount = await db.collection('q-auth').findOne({ $or: [{ _id: accountInputWithId._id }, { email: accountInputWithId.email }] })
      if (existingAccount) {
        return existingAccount
      } else {
        accountInputWithId.password = await new Promise((resolve, reject) => {
          bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(accountInputWithId.password, salt, function (err, hash) {
              if (err) reject(err)
              resolve(hash)
            })
          })
        })
        const createdAccount = await db.collection('q-auth').insertOne(accountInputWithId)
        return createdAccount.ops[0]
      }
    },
    modifyPassword: async (_parent, args, context, _info) => {
      const { accountInputWithId } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == accountInputWithId._id || user.accountType == 'ADMIN') {
        accountInputWithId.password = await new Promise((resolve, reject) => {
          bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(accountInputWithId.password, salt, function (err, hash) {
              if (err) reject(err)
              resolve(hash)
            })
          })
        })
        const modifiedAccountPassword = await db.collection('q-auth').findOneAndUpdate({ _id: accountInputWithId._id, email: accountInputWithId.email }, { $set: { password: accountInputWithId.password } }, { returnOriginal: false })
        return modifiedAccountPassword.value
      } else {
        return null
      }
    },
    deleteAuthAccount: async (_parent, args, _context, _info) => {
      const { _id } = args
      await db.collection('q-auth').deleteOne({ _id }, (err, result) => {
        console.log(`Removed document with accountID: ${_id}`)
        return result
      })
    }
  },
}
